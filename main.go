package main

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"hash/crc32"
	"image"
	"image/color"
	"image/png"
	"io/ioutil"
	"math"
	"os"

	"github.com/sirupsen/logrus"

	"gopkg.in/urfave/cli.v2"
)

func main() {
	app := &cli.App{
		Name:  "image-encode",
		Usage: "encode text into images",
	}
	logrus.SetLevel(logrus.DebugLevel)
	app.OnUsageError = func(c *cli.Context, err error, _ bool) error {
		logrus.Error(err)
		return nil
	}
	app.Action = func(c *cli.Context) error {
		text := ""
		if c.NArg() == 0 {
			logrus.Print("Enter your message:")
			data, err := ioutil.ReadAll(os.Stdin)
			if err != nil {
				return err
			}
			text = string(data)
		} else {
			fil := c.Args().First()
			logrus.Infof("Reading file %q", fil)
			data, err := ioutil.ReadFile(fil)
			if err != nil {
				return err
			}
			logrus.Infof("Got %d bytes of data", len(data))
			text = string(data)
		}
		logrus.Debug("Encoding image...")
		out := encode(text, c.Int("height"))
		logrus.Debugf("Image encoded with %dx%d", out.Bounds().Dx(), out.Bounds().Dy())
		buf := bytes.NewBuffer(nil)
		if err := png.Encode(buf, out); err != nil {
			return err
		}
		logrus.Debugf("Image size %d bytes", buf.Len())
		return ioutil.WriteFile(c.String("output"), buf.Bytes(), 0660)
	}
	app.Commands = []*cli.Command{
		&cli.Command{
			Name: "decode",
			Action: func(c *cli.Context) error {
				var (
					data []byte
					err  error
				)
				if c.NArg() == 0 {
					data, err = ioutil.ReadAll(os.Stdin)
					if err != nil {
						return err
					}
				} else {
					fil := c.Args().First()
					data, err = ioutil.ReadFile(fil)
					if err != nil {
						return err
					}
				}
				img, err := png.Decode(bytes.NewReader(data))
				if err != nil {
					return err
				}
				if t, err := decode(img); err != nil {
					return err
				} else {
					fmt.Printf(
						"--- BEGIN DECODED TEXT ---\n%s\n--- END DECODED TEXT ---\n",
						t)
				}
				return nil
			},
		},
	}
	app.Flags = []cli.Flag{
		&cli.IntFlag{
			Name:  "height",
			Value: 32,
			Usage: "Image height in pixels",
		},
		&cli.StringFlag{
			Name:    "output",
			Aliases: []string{"o"},
			Value:   "out.png",
			Usage:   "Output Image in PNG Format",
		},
	}
	app.Version = "0.0.1"

	app.ArgsUsage = "<filename>"

	err := app.Run(os.Args)
	if err != nil {
		logrus.Error(err)
		return
	}
}

func encode(text string, width int) image.Image {
	data := []byte(text)
	{
		checksum := crc32.ChecksumIEEE(data)
		chkHeader := make([]byte, 4)
		binary.BigEndian.PutUint32(chkHeader, checksum)
		data = append(data, chkHeader...)
		logrus.Debugf("Checksum is %X", checksum)
	}
	bytesOfData := uint16(len(data))
	{
		lenHeader := make([]byte, 2)
		binary.BigEndian.PutUint16(lenHeader, bytesOfData)
		data = append(lenHeader, data...)
	}
	logrus.Debugf("Encoding %d bytes of data with metadata", len(data))
	if len(data) < 32 {
		logrus.Debugf("Encoding: %x", data)
	}
	lines := [][]byte{}
	for i := 0; i < len(data); i += width * 3 {
		var newLine []byte
		if i+(width*3) < len(data) {
			newLine = data[i : i+(width*3)]
		} else {
			newLine = data[i:]
		}
		lines = append(lines, newLine)
	}
	logrus.Debugf("Got %d lines of data", len(lines))
	// 3 bytes of Data per Width, Rounded Up
	heightFloat := math.Ceil((float64(bytesOfData) / float64(width)) / 3.0)
	height := int(heightFloat)
	logrus.Debugf("Calculated : %d, %f", height, heightFloat)
	logrus.Debugf("Image size calculated as %dx%d", height, width)
	img := image.NewRGBA(image.Rect(0, 0, width, height))
	var col color.RGBA
	col.A = 255
	for k := range lines {
		logrus.Debugf("Writing line of lenght %d, %d pixels", len(lines[k]), len(lines[k])/3)
		for j := 0; j < len(lines[k]); j++ {
			// 3 channel data, not Alpha Channel
			switch j % 3 {
			case 0:
				col.R = lines[k][j]
			case 1:
				col.G = lines[k][j]
			case 2:
				col.B = lines[k][j]
				y := k
				x := (j - 2) / 3
				logrus.Debugf("Writing Color at %d/%d: %x", x, y, col)
				img.SetRGBA(x, y, col)
				col.A = 255
				col.R = 0
				col.G = 0
				col.B = 0
			}
		}
		y := k
		x := (len(lines[k])-2)/3 + 1
		logrus.Debugf("Writing Color at Line End %d/%d: %x", x, y, col)
		img.SetRGBA(x, y, col)
	}
	return img
}

func decode(img image.Image) (text string, err error) {
	data := []byte{}
	bounds := img.Bounds()
	for k := 0; k < bounds.Dx(); k++ {
		for j := 0; j < bounds.Dy(); j++ {
			r, g, b, _ := img.At(k, j).RGBA()
			logrus.Debugf("Data at %d/%d: %x", j, k, img.At(k, j))
			data = append(data, byte(r), byte(g), byte(b))
		}
	}
	logrus.Debugf("Finished decode with %d bytes of data", len(data))
	lengthOfData := binary.BigEndian.Uint16(data[:2])
	logrus.Debugf("Header indicates %d bytes", int(lengthOfData))
	data = data[2:]
	if int(lengthOfData) > len(data) {
		return "", errors.New("Image indicates length larger than data available")
	}
	data = data[:lengthOfData]
	if len(data) < 64 {
		logrus.Debugf("Data: %x", data)
	}
	logrus.Debugf("Handling data of %d length", len(data))
	if len(data) == 0 {
		return "", errors.New("empty data")
	}
	chkSumRaw := data[lengthOfData-4:]
	chkSum := binary.BigEndian.Uint32(chkSumRaw)
	data = data[:len(data)-4]
	chkSumReal := crc32.ChecksumIEEE(data)
	if chkSum != chkSumReal {
		logrus.Errorf("Wanted %X got %X in checksum",
			chkSum, chkSumReal)
		return "", errors.New("Checksum failure")
	}
	logrus.Infof("Checksum is %X", chkSum)
	return string(data), nil
}
